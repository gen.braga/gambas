#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1 Fcdplayer.form:16
msgid "CDplayer"
msgstr "-"

#: .project:2
msgid "A Tiny CDplayer"
msgstr "Lehký CDpřehlávač"

#: Fcdplayer.class:22
msgid "I Could not load cd-rom drive"
msgstr "Nemohu načíst CD-ROM"

#: Fcdplayer.class:26
msgid "Your PC does not have cd-rom drive"
msgstr "Váš počítač nemá cd-rom mechaniku"

#: Fcdplayer.form:22
msgid "Track"
msgstr "Stopa"

#: Fcdplayer.form:28
msgid "&Play"
msgstr "-"

#: Fcdplayer.form:33
msgid "&Eject"
msgstr "&Vysunout"

#: Fcdplayer.form:43
msgid "&Stop"
msgstr "-"

#: Fcdplayer.form:48
msgid "Play &Track"
msgstr "&Přehrát stopu"

#: Fcdplayer.form:57
msgid "Volume"
msgstr "Hlasitost"
