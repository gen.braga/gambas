# Catalan translation of RobotFindsKitten
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as the RobotFindsKitten package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#
msgid ""
msgstr ""
"Project-Id-Version: RobotFindsKitten\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2011-03-20 21:58+0100\n"
"Last-Translator: Jordi Sayol <g.sayol@yahoo.es>\n"
"Language-Team: \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Catalan\n"

#: .project:1
msgid "gambrfk"
msgstr "-"

#: Frfk.class:180
msgid "You found kitten! Way to go, robot!"
msgstr "Heu trobat el gatet! Ben fet robot!"

#: Frfk.class:203
msgid "robotfindskitten"
msgstr "robottrobaelgatet"

#: Frfk.class:211
msgid "Esc"
msgstr "Esc"

#: Frfk.class:231
msgid "In this game, you are robot (#). Your job is to find kitten. This task is complicated by the existence of various things which are not kitten. Robot must touch items to determine if they are kitten or not. The game ends when robot finds kitten. Alternatively, you may end the game by hitting the Esc key.\n"
msgstr "En aquest joc, sou el robot (#). La vostra missió es trobar el gatet. Aquesta tasca es complica per l'existència de diverses coses que no son el gatet. El robot ha de tocar les coses per determinar si son el gatet o no. El joc acaba quan el robot troba el gatet. També podeu acabar el joc tot prement la tecla Esc.\n"

